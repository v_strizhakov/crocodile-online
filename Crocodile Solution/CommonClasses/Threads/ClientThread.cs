﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using Common.Extensions;
using Common.Helpers;
using Common.Managers;
using Common.Protocol;
using Common.Protocol.WebSocket;
using Common.Services;
using GalaSoft.MvvmLight.Ioc;
using Newtonsoft.Json;

namespace Common.Threads
{
	public class ClientThread
	{
		#region Private Fields

		private bool _isWebSocket = false;
		private TcpClient _client;
		private IRequestProcessor _requestProcessor;
        private LogService _logService;

		#endregion

		#region Life Cycle

		public ClientThread(TcpClient client, IRequestProcessor requestProcessor)
		{
			_client = client;
			_requestProcessor = requestProcessor;
            _logService = SimpleIoc.Default.GetInstance<LogService>();
		}

		#endregion

		#region Static Private Methods

		static private byte[] GetBytesFromMemoryStream(MemoryStream memoryStream)
		{
			byte[] bytes = new byte[memoryStream.Length];
			byte[] buffer = memoryStream.GetBuffer();
			for (int i = 0; i < bytes.Length; i++)
			{
				bytes[i] = buffer[i];
			}
			return bytes;
		}

		#endregion

		#region Public Methods

		public void Run()
		{
			String ip = _client.Client.RemoteEndPoint.ToString();
			MemoryStream memoryStream = new MemoryStream();
			const int bufSize = 2048;
			byte[] buf = new byte[bufSize];
			NetworkStream networkStream = null;
			try
			{
				networkStream = _client.GetStream();

                // byte[] bt = Encoding.UTF8.GetBytes("LOL");
                 //   networkStream.Write(bt, 0, bt.Length);

                while (true)
				{
					do
					{
						int count = networkStream.Read(buf, 0, bufSize);
						if (count == 0)
						{
							throw new Exception("0 bytes received");
						}
						memoryStream.Write(buf, 0, count);
					} while (networkStream.DataAvailable);

					byte[] requestBytes = GetBytesFromMemoryStream(memoryStream);
					memoryStream.SetLength(0);

                    //"Name: CAPIP\r\nVersion: 0.0\r\nMethod-Path: auth.login\r\n\r\n{\"username\":\"123\",\"password\":\"456\"}"
                    //"GET / HTTP/1.1\r\nHost: 127.0.0.1:12701\r\nConnection: Upgrade\r\nPragma: no-cache\r\nCache-Control: no-cache\r\nUser-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36\r\nUpgrade: websocket\r\nOrigin: file://\r\nSec-WebSocket-Version: 13\r\nAccept-Encoding: gzip, deflate, br\r\nAccept-Language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7\r\nSec-WebSocket-Key: J5mqNYicKU74m6EI+wEcnQ==\r\nSec-WebSocket-Extensions: permessage-deflate; client_max_window_bits\r\n\r\n"
                    string requestString = Encoding.UTF8.GetString(requestBytes, 0, requestBytes.Length);

                  

                    if (Regex.IsMatch(requestString, "^GET"))
					{
						Byte[] answer = Encoding.UTF8.GetBytes("HTTP/1.1 101 Switching Protocols" + ConstantsManager.Protocol.EndOfLine
							+ "Connection: Upgrade" + ConstantsManager.Protocol.EndOfLine
							+ "Upgrade: websocket" + ConstantsManager.Protocol.EndOfLine
							+ "Sec-WebSocket-Accept: " + Convert.ToBase64String(
								System.Security.Cryptography.SHA1.Create().ComputeHash(
									Encoding.UTF8.GetBytes(
										new Regex("Sec-WebSocket-Key: (.*)").Match(requestString).Groups[1].Value.Trim() + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
									)
								)
							) + ConstantsManager.Protocol.EndOfLine
							+ ConstantsManager.Protocol.EndOfLine);
						networkStream.Write(answer, 0, answer.Length);
						_isWebSocket = true;//??
						continue;//??
					}

					if (_isWebSocket)//??
					{
						Frame frame = Frame.Decode(requestBytes);
						requestBytes = frame.ByteData;
						requestString = Encoding.UTF8.GetString(requestBytes, 0, requestBytes.Length);
					}

					Request request = Request.CreateFromString(requestString);
					Response response;
					byte[] responseBytes = new byte[0];

					try
					{
						if (false)
						{
							//Field[] requiredFileds = new Field[]
							//{
							//	new Field { Content = "public_key", IsRequired = true, CanBeEmpty = false }
							//};

							//string version = request.Headers[ConstantsManager.Protocol.Header.Version];
							//switch (version)
							//{
							//	case "0.0":
							//		{
							//			Dictionary<string, string> fields = Validator.ValidateFromJSON(request.JsonData, requiredFileds);
							//			string clientPublicKey = fields["public_key"];
							//			_cryptoService = new CryptoService(clientPublicKey, out string serverPublicKey, false);
							//			fields["public_key"] = serverPublicKey;
							//			response = Response.CreateSuccessfullWidthData(fields);
							//			break;
							//		}
							//	default:
							//		throw new UnknownVersionException(version);
							//}
						}
						else
						{
							//request.DecryptJsonData(_cryptoService);
							response = _requestProcessor.ProcessRequest(request);
							//response.EncryptJsonData(_cryptoService);
						}
					}
					catch (RequestProcessorException ex)
					{
						response = Response.CreateFailureWithError(JsonConvert.SerializeObject(ex.Message));
					}

					string responseString = response.ToString();
					if (_isWebSocket)
					{
						responseBytes = Frame.Encode(responseString);
					}
					else
					{
						responseBytes = Encoding.UTF8.GetBytes(responseString);
					}

					networkStream.Write(responseBytes, 0, responseBytes.Length);
					_logService.Log("{0} was sent to {1}", response.JsonData, ip);
				}
			}
			catch (Exception ex)
			{
                _logService.Log("The exception was thrown in client thread ({0}):\n\t{1}", ip, ex.Message);
			}
			finally
			{
				networkStream?.Close();
				memoryStream?.Close();
			}
		}

		#endregion
	}
}
