﻿using Common.Helpers;
using Common.Protocol;
using Common.Services;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using GalaSoft.MvvmLight.Ioc;

namespace Common.Threads
{
	public class ServerThread
	{
        private IPEndPoint _ip;
		private IRequestProcessorFactory _requestProcessorFactory;
        private LogService _logService;

        public ServerThread(IPEndPoint ip)
		{
            _ip = ip;
            _requestProcessorFactory = SimpleIoc.Default.GetInstance<IRequestProcessorFactory>();
            _logService = SimpleIoc.Default.GetInstance<LogService>();
        }

		public void Run()
		{
			try
			{
				TcpListener server = new TcpListener(_ip);
				server.Start();
                _logService.Log("Server was launched on {0}:{1}", _ip.Address, _ip.Port);

				while (true)
				{
					TcpClient client = server.AcceptTcpClient();
                    _logService.Log("{0} was connected to the server", client.Client.RemoteEndPoint);
					ClientThread clientThread = new ClientThread(client, _requestProcessorFactory.CreateRequestProcessor(client));
					Thread thread = new Thread(clientThread.Run);
					thread.IsBackground = true;
					thread.Start();					
				}
			}
			catch (Exception ex)
			{
                _logService.Log("The exception was thrown while the server was running:\n\t{0}", ex.Message);
			}
		}
	}
}
