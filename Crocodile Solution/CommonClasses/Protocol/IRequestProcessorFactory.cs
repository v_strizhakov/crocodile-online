﻿using System.Net.Sockets;

namespace Common.Protocol
{
	public interface IRequestProcessorFactory
	{
		IRequestProcessor CreateRequestProcessor(TcpClient client);
	}
}
