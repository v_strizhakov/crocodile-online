﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Protocol.WebSocket
{
	public class Frame
	{
		#region Private Fields

		private UInt64 _length;
		private byte[] _byteData;

		#endregion

		#region Public Properties

		public byte[] ByteData => _byteData;

		#endregion

		#region Life Cycle

		private Frame(UInt64 length, byte[] byteData)
		{
			_length = length;
			_byteData = byteData;
		}

		#endregion

		#region Static Public Methods

		static public byte[] Encode(string stringData)
		{
			int length = stringData.Length;
			List<byte> encodedBytes = new List<byte>();
			encodedBytes.Add(0x81);
			if (length <= 125)
			{
				encodedBytes.Add((byte)length);
			}
			else if (length >= 126 && length <= 65535)
			{
				encodedBytes.Add(126);
				encodedBytes.Add((byte)((length >> 8) & 255));
				encodedBytes.Add((byte)((length) & 255));
				//encodedBytes.AddRange(BitConverter.GetBytes((short)length));
			}
			else
			{
				encodedBytes.Add(127);
				encodedBytes.AddRange(BitConverter.GetBytes((UInt64)length));
			}
			encodedBytes.AddRange(Encoding.ASCII.GetBytes(stringData));
			return encodedBytes.ToArray();
		}

		static public Frame Decode(byte[] bytes)
		{
			UInt64 totalLength = 0;
			List<byte[]> frameParts = new List<byte[]>();
			bool isLastPart = true;
			UInt64 offset = 0;

			do
			{
				int firstByte = bytes[offset];
				if (firstByte != 0x81)
				{
					// TODO: Create UnknownFrameFormatException
					//throw new Exception("Unknown WebSocket-frame format");
				}

				isLastPart = (bytes[offset] & (1 << 0)) == 1;

				UInt64 length = (UInt64)(bytes[offset + 1] - 0x80);
				UInt64 lengthBytesCount = 1;
				if (length == 126)
				{
					length = BitConverter.ToUInt16(new byte[] { bytes[offset + 3], bytes[offset + 2] }, 0);
					lengthBytesCount = 3;
				}
				else if (length == 127)
				{
					length = BitConverter.ToUInt64(new byte[] { bytes[offset + 2], bytes[offset + 3], bytes[offset + 4], bytes[offset + 5],
															bytes[offset + 6], bytes[offset + 7], bytes[offset + 8], bytes[offset + 9] }, 0);
					lengthBytesCount = 8;
				}
				totalLength += length;

				byte[] mask = new byte[] { bytes[offset + lengthBytesCount + 1], bytes[offset + lengthBytesCount + 2],
										   bytes[offset + lengthBytesCount + 3], bytes[offset + lengthBytesCount + 4] };
				byte[] decoded = new byte[length];

				for (UInt64 i = 0; i < length; i++)
				{
					UInt64 realPosition = offset + lengthBytesCount + 5 + i;
					decoded[i] = (byte)(bytes[realPosition] ^ mask[i % 4]);
				}
				frameParts.Add(decoded);
				offset += length;
			} while (!isLastPart);

			byte[] byteData = new byte[totalLength];
			int index = 0;
			foreach (byte[] framePart in frameParts)
			{
				framePart.CopyTo(byteData, index);
				index += framePart.Length;
			}

			return new Frame(totalLength, byteData);
		}

		#endregion
	}
}
