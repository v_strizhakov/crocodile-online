﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Common.Managers;
using Common.Extensions;
using Common.Services;

namespace Common.Protocol
{
	public class IPackage
	{
		#region Protected Fields

		protected string _jsonData;

		#endregion

		#region Public Properties

		public HeaderCollection Headers;
		public string JsonData
		{
			get => _jsonData;
			private set => _jsonData = value;
		}

		#endregion

		#region Life Cycle

		protected IPackage(HeaderCollection headers, string jsonData)
		{
			Headers = headers;
			_jsonData = jsonData;
		}

		#endregion

		#region Static Public Methods

		static public IPackage CreateFromString(string requestString, string[] allowedHeaders)
		{
			int emptyLineIndex = requestString.IndexOf(ConstantsManager.Protocol.EmptyLine);
			if (emptyLineIndex == -1)
			{
				throw new InvalidProtocolStructureException("Empty-line doesn't exist.");
			}

			string headersSection = requestString.Substring(0, emptyLineIndex + 2);
			string dataSection = requestString.Substring(emptyLineIndex + ConstantsManager.Protocol.EmptyLine.Length, requestString.Length - emptyLineIndex - ConstantsManager.Protocol.EmptyLine.Length);

			string[] headersStrings = headersSection.Split(new string[] { ConstantsManager.Protocol.EndOfLine }, StringSplitOptions.RemoveEmptyEntries);

			HeaderCollection headers = new HeaderCollection();
			for (int i = 0; i < headersStrings.Length; i++)
			{
				if (!Regex.IsMatch(headersStrings[i], "^.*: .*$"))
				{
					throw new InvalidProtocolStructureException("Request header line have invalid format");
				}

				string[] keyValue = headersStrings[i].Split(new string[] { ": " }, StringSplitOptions.None);
				string key = keyValue[0];
				string value = keyValue[1];

				if (!allowedHeaders.Contains(key))
				{
					throw new InvalidProtocolStructureException(String.Format("Invalid header name: {0}", key));
				}

				headers.Add(new Header(key, value));
			}

			foreach (string headerKey in allowedHeaders)
			{
				if (!headers.ContainsKey(headerKey))
				{
					throw new InvalidProtocolStructureException(String.Format("Request doesn't have {0} header", headerKey));
				}
			}

			return new IPackage(headers, dataSection);
		}
		#endregion

		#region Public Methods

		public override string ToString()
		{
			string result = String.Empty;
			int i = 0;
			foreach (Header header in Headers)
			{
				if (i++ == Headers.Count)
				{
					break;
				}
				result += String.Format("{0}: {1}" + ConstantsManager.Protocol.EndOfLine, header.Key, header.Value);
			}
			result += String.Format(ConstantsManager.Protocol.EndOfLine + "{0}", _jsonData);
			return result;
		}

		public void DecryptJsonData(CryptoService cryptoService)
		{
			byte[] encryptedData = Encoding.UTF8.GetBytes(JsonData);
			byte[] decryptedData = cryptoService.Decrypt(encryptedData);
			JsonData = Encoding.UTF8.GetString(decryptedData);
		}

		public void EncryptJsonData(CryptoService cryptoService)
		{
			byte[] decryptedData = Encoding.UTF8.GetBytes(JsonData);
			byte[] encryptedData = cryptoService.Encrypt(decryptedData);
			JsonData = Encoding.UTF8.GetString(encryptedData);
		}

		#endregion
	}
}
