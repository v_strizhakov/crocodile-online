﻿using Common.Helpers;
using Common.Managers;
using System.Linq;

namespace Common.Protocol
{
	public class Request : IPackage
	{
		#region Static Private Fields

		static private string[] _allowedHeaders = new string[]
		{
			ConstantsManager.Protocol.Header.Name,
			ConstantsManager.Protocol.Header.Version,
			ConstantsManager.Protocol.Header.MethodPath
		};

		#endregion

		#region Life Cycle

		protected Request(HeaderCollection headers, string data) : base(headers, data) { }

		#endregion

		#region Static Public Methods

		static public Request Create(string methodPath, string data)
		{
			HeaderCollection headers = new HeaderCollection();
			headers.Add(new Header(ConstantsManager.Protocol.Header.Name, ConstantsManager.Protocol.Name));
			headers.Add(new Header(ConstantsManager.Protocol.Header.Version, ConstantsManager.Protocol.Versions.Last()));
			headers.Add(new Header(ConstantsManager.Protocol.Header.MethodPath, methodPath));

			return new Request(headers, data);
		}

		static public Request CreateFromString(string requestString)
		{
			IPackage pkg = CreateFromString(requestString, _allowedHeaders);
			return new Request(pkg.Headers, pkg.JsonData);
		}

		#endregion
	}
}