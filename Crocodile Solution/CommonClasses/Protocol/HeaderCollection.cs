﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Common.Protocol
{
	public class HeaderCollection : IEnumerable
	{
		#region Private Fields

		List<Header> _headers = new List<Header>();

		#endregion

		#region Public Properties

		public int Count => _headers.Count;

		#endregion

		#region Public Methods

		public void Add(Header header)
		{
			Header tmp = _headers.Find(x => x.Key == header.Key);
			if (tmp != null)
			{
				tmp.Value = header.Value;
			}
			else
			{
				_headers.Add(header);
			}
		}

		public bool ContainsKey(string key)
		{
			return _headers.Find(x => x.Key == key) != null;
		}

		public String this[string key]
		{
			get => _headers.Find(x => x.Key == key).Value;
			set => _headers.Find(x => x.Key == key).Value = value;
		}

		#endregion

		#region IEnumerable

		public IEnumerator GetEnumerator()
		{
			return ((IEnumerable)_headers).GetEnumerator();
		}

		#endregion
	}
}
