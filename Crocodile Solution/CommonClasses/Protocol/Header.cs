﻿namespace Common.Protocol
{
	public class Header
	{
		#region Public Properties

		public string Key { get; set; }
		public string Value { get; set; }

		#endregion

		#region Life Cycle

		public Header(string key, string value)
		{
			Key = key;
			Value = value;
		}

		#endregion
	}
}
