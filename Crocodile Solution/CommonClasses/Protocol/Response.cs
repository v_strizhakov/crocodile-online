﻿using Common.Managers;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace Common.Protocol
{
	public class Response : IPackage
	{
		#region Static Private Fields

		static private string[] _allowedHeaders = new string[]
		{
			ConstantsManager.Protocol.Header.Name,
			ConstantsManager.Protocol.Header.Version,
			ConstantsManager.Protocol.Header.ResultString
		};

		#endregion

		#region Life Cycle

		protected Response(HeaderCollection headers, string data) : base(headers, data) { }

		#endregion

		#region Static Public Methods

		static public Response CreateSuccessfullWidthData(object data)
		{
			HeaderCollection headers = new HeaderCollection();
			headers.Add(new Header(ConstantsManager.Protocol.Header.Name, ConstantsManager.Protocol.Name));
			headers.Add(new Header(ConstantsManager.Protocol.Header.Version, ConstantsManager.Protocol.Versions.Last()));
			headers.Add(new Header(ConstantsManager.Protocol.Header.ResultString, ConstantsManager.Protocol.Result.Ok));

			return new Response(headers, JsonConvert.SerializeObject(data));
		}

		static public Response CreateFailureWithError(string error)
		{
			HeaderCollection headers = new HeaderCollection();
			headers.Add(new Header(ConstantsManager.Protocol.Header.Name, ConstantsManager.Protocol.Name));
			headers.Add(new Header(ConstantsManager.Protocol.Header.Version, ConstantsManager.Protocol.Versions.Last()));
			headers.Add(new Header(ConstantsManager.Protocol.Header.ResultString, ConstantsManager.Protocol.Result.Error));

			return new Response(headers, JsonConvert.SerializeObject(new KeyValuePair<string, string>("message", error)));
		}

		#endregion
	}
}
