﻿namespace Common.Protocol
{
	public interface IRequestProcessor
	{
		Response ProcessRequest(object requestObject);
	}
}
