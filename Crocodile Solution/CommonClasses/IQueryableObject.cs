﻿using System.Data;

namespace Common
{
    public interface IQueryableObject
    {
        void FromDataRow(DataRow row);
    }
}
