﻿namespace Common.Managers
{
	static public class ConstantsManager
	{
		static public class Protocol
		{
			public const string Name = "CAPIP";
			public const string EmptyLine = "\r\n\r\n";
			public const string EndOfLine = "\r\n";
			static public readonly string[] Versions = new string[] { "0.0" };

			static public class Header
			{
				static public readonly string Name = "Name";
				static public readonly string Version = "Version";
				static public readonly string MethodPath = "Method-Path";
				static public readonly string ResultString = "Result-String";
			}

			static public class Result
			{
				static public readonly string Ok = "Ok";
				static public readonly string Error = "Error";
			}
		}

        static public class File
        {
            static public class Extensions
            {
                public const string Log = ".log";
            }
        }
	}
}
