﻿using Common.Extensions;
using Common.Helpers;
using Common.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Common.Helpers
{
	class Validator
	{
		#region Static Public Methods

		static public Dictionary<string, string> ValidateFromJSON(string json, Field[] requiredFields)
		{
			JObject jsonObject = JsonConvert.DeserializeObject(json) as JObject;
			Dictionary<string, string> fields = new Dictionary<string, string>();
			foreach (Field field in requiredFields)
			{
				if (jsonObject.ContainsKey(field))
				{
					string data = jsonObject.Property(field).Value.ToString().Trim();
					if (data == String.Empty && !field.CanBeEmpty)
					{
						throw new ParameterCanNotBeEmptyException(field);
					}
					fields.Add(field, data);
				}
				else if (field.IsRequired)
				{
					throw new ParameterIsExpectingException(field);
				}
			}
			return fields;
		}

		#endregion
	}
}
