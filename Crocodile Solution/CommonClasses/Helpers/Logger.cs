﻿using System;
using System.IO;
using System.Threading;

namespace Common.Helpers
{
	static public class Logger
	{
		#region Private Fields

		static private bool _isInitialized = false;
		static private Mutex _mutex = new Mutex();
		static private StreamWriter _streamWriter;
		static private bool _isNeedToLogInConsole = true;

		#endregion

		#region Public Methods

		static public void SetLogInConsole(bool isNeed)
		{
			_isNeedToLogInConsole = isNeed;
		}

		static public void Initialize(string fileName)
		{
			if (_isInitialized)
			{
				return;
			}

			if (_streamWriter != null)
			{
				_streamWriter.Close();
				_streamWriter = null;
			}

			FileStream FS = new FileStream(fileName, FileMode.Append, FileAccess.Write);
			_streamWriter = new StreamWriter(FS);
			_isInitialized = true;
			Log("Log file was created: {0}", fileName);
		}

		static public void Log(object obj, params object[] objs)
		{
			_mutex.WaitOne();

			if (!_isInitialized)
			{
				throw new Exception("Trying to call Logger.Log(object obj, params object[] objs) without being initialized");
			}

            string time = DateTime.Now.ToString("HH:mm:ss");
			if (objs.Length == 0)
			{
				_streamWriter.WriteLine(time + ": " + obj);
				if (_isNeedToLogInConsole)
				{
					Console.WriteLine(time + ": " + obj);
				}
			}
			else
			{
				_streamWriter.WriteLine(time + ": " + obj.ToString(), objs);
				if (_isNeedToLogInConsole)
				{
					Console.WriteLine(time + ": " + obj.ToString(), objs);
				}
			}

			_mutex.ReleaseMutex();
		}

		static public void Uninitialize()
		{
			if (_streamWriter != null)
			{
				_streamWriter.Close();
			}
		}

		#endregion
	}
}
