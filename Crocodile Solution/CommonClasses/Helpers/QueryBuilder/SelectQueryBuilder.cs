﻿using System;
using System.Text;

namespace Common.Queries
{
    public class SelectQueryBuilder : BaseQueryBuilder
    {
        #region Private Fields
        
        private string _select = "*";
        private string _from;
        private string _where;
        private string _groupBy;
        private string _orderBy;

        #endregion

        #region Public Methods

        public SelectQueryBuilder Select(params string[] whats)
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < whats.Length; i++)
            {
                if (i > 0)
                {
                    stringBuilder.Append(", ");
                }
                string parameter = whats[i].Trim();
                stringBuilder.Append(String.Format((parameter == "*") ? "{0}" : "'{0}'", parameter));
            }
            _select = stringBuilder.ToString();
            return this;
        }

        public SelectQueryBuilder From(params string[] tables)
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < tables.Length; i++)
            {
                if (i > 0)
                {
                    stringBuilder.Append(", ");
                }
                string table = tables[i].Trim();
                stringBuilder.Append($"{table}");
            }
            _from = stringBuilder.ToString();
            return this;
        }

        public SelectQueryBuilder Where(string firstParam, string condition, string secondParam)
        {
            _where = String.Format("{0} {1} '{2}' ", firstParam.Trim(), condition.Trim(), secondParam.Trim());
            return this;
        }

        public SelectQueryBuilder AndWhere(string firstParam, string condition, string secondParam)
        {
            if (!String.IsNullOrEmpty(_where))
            {
                _where += "AND ";
            }
            _where += String.Format("{0} {1} '{2}' ", firstParam.Trim(), condition.Trim(), secondParam.Trim());
            return this;
        }

        public SelectQueryBuilder OrWhere(string firstParam, string condition, string secondParam)
        {
            if (!String.IsNullOrEmpty(_where))
            {
                _where += "OR ";
            }
            _where += String.Format("{0} {1} '{2}' ", firstParam.Trim(), condition.Trim(), secondParam.Trim());
            return this;
        }

        public SelectQueryBuilder GroupBy(params string[] groups)
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < groups.Length; i++)
            {
                if (i > 0)
                {
                    stringBuilder.Append(", ");
                }
                string group = groups[i].Trim();
                stringBuilder.Append(String.Format("'{0}'", group));
            }
            _groupBy = stringBuilder.ToString();
            return this;
        }

        public SelectQueryBuilder OrderBy(params string[] orders)
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < orders.Length; i++)
            {
                if (i > 0)
                {
                    stringBuilder.Append(", ");
                }
                string order = orders[i].Trim();
                stringBuilder.Append(String.Format("'{0}'", order));
            }
            _orderBy = stringBuilder.ToString();
            return this;
        }

        #endregion

        #region BaseQueryBuilder

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(String.Format("SELECT {0} FROM {1} ", _select, _from));
            if (!String.IsNullOrEmpty(_where))
            {
                stringBuilder.Append(String.Format("WHERE {0} ", _where));
            }
            if (!String.IsNullOrEmpty(_groupBy))
            {
                stringBuilder.Append(String.Format("GROUP BY {0} ", _groupBy));
            }
            if (!String.IsNullOrEmpty(_orderBy))
            {
                stringBuilder.Append(String.Format("ORDER BY {0} ", _orderBy));
            }
            return stringBuilder.ToString();
        }

        #endregion
    }
}
