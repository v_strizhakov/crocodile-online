﻿namespace Common.Queries
{
    public class QueryBuilderFactory
    {
        #region Static Public Methods

        static public SelectQueryBuilder CreateSelectQueryBuilder()
        {
            return new SelectQueryBuilder();
        }

        static public InsertQueryBuilder CreateInsertQueryBuilder()
        {
            return new InsertQueryBuilder();
        }

        static public UpdateQueryBuilder CreateUpdateQueryBuilder()
        {
            return new UpdateQueryBuilder();
        }

        static public DeleteQueryBuilder CreateDeleteQueryBuilder()
        {
            return new DeleteQueryBuilder();
        }

        #endregion
    }
}
