﻿namespace Common.Queries
{
    public abstract class BaseQueryBuilder
    {
        public abstract override string ToString();
    }
}
