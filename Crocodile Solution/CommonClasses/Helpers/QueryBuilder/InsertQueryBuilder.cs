﻿using System.Text;

namespace Common.Queries
{
    public class InsertQueryBuilder : BaseQueryBuilder
    {
        #region Private Fields

        private string _columns;
        private string _values;
        private string _insertInto;

        #endregion

        #region Public Methods

        public InsertQueryBuilder InsertInto(string table)
        {
            _insertInto = table;
            return this;
        }

        public InsertQueryBuilder Columns(params string[] columns)
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < columns.Length; i++)
            {
                if (i > 0)
                {
                    stringBuilder.Append(", ");
                }
                stringBuilder.Append(columns[i]);
            }
            _columns = stringBuilder.ToString();
            return this;
        }

        public InsertQueryBuilder Values(params string[] values)
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < values.Length; i++)
            {
                if (i > 0)
                {
                    stringBuilder.Append(", ");
                }
                stringBuilder.Append($"'{values[i]}'");
            }
            _values = stringBuilder.ToString();
            return this;
        }

        #endregion

        #region BaseQueryBuilder

        public override string ToString()
        {
            return $"INSERT INTO {_insertInto} ({_columns}) VALUES({_values})";
        }

        #endregion
    }
}
