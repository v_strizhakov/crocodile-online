﻿using System.Globalization;

namespace Common.Helpers
{
	static public class NameRecreator
	{
		static public string ToCSharpName(string name)
		{
			name = name.Trim().ToLower().Replace('_', ' ');
			return new CultureInfo("en-US").TextInfo.ToTitleCase(name).Replace("_", "");
		}
	}
}
