﻿using System;

namespace Common.Extensions
{
	public class InvalidProtocolStructureException : Exception
	{
		public InvalidProtocolStructureException(string message) : base("Package doesn't fit CAPIP: " + message) { }
	}

	public class RequestProcessorException : Exception
	{
		public RequestProcessorException(string message) : base(message) { }
	}

	public class UnknownProtocolNameException : RequestProcessorException
	{
		public UnknownProtocolNameException(string protocolName) : base("Unknown protocol name: " + protocolName) { }
	}

	public class UnknownMethodPathException : RequestProcessorException
	{
		public UnknownMethodPathException(string methodPath) : base("Unknown method path: " + methodPath) { }
	}

	public class UnknownVersionException : RequestProcessorException
	{
		public UnknownVersionException(string version) : base("Unknown protocol version: " + version) { }
	}

	public class ParameterIsExpectingException : RequestProcessorException
	{
		public ParameterIsExpectingException(string parameter) : base(parameter + " parameter is expecting") { }
	}

	public class ParameterCanNotBeEmptyException : RequestProcessorException
	{
		public ParameterCanNotBeEmptyException(string parameter) : base(parameter + " parameter can't be empty") { }
	}
}
