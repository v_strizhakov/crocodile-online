﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Common.Models
{
    [DatabaseSerializable]
	public class User : IQueryableObject
	{
		#region Public Properties

        [JsonProperty("id")]
        [DatabaseProperty("id")]
		public int Id { get; set; }

        [JsonProperty("username")]
        [DatabaseProperty("username")]
		public string Username { get; set; }

        [JsonProperty("email")]
        [DatabaseProperty("email")]
		public string Email { get; set; }

        [JsonProperty("password")]
        [DatabaseProperty("password")]
		public string Password { get; set; }

        [JsonProperty("balance_id")]
        [DatabaseProperty("balance_id")]
		public int BalanceId { get; set; }

        #endregion

        #region IQueryableObject

        public void FromDataRow(DataRow row)
        {
            Id = Int32.Parse(row["id"].ToString());
            Username = row["username"].ToString();
            Password = row["password"].ToString();
            Email = row["email"].ToString();
            // BalanceId = Int32.Parse(row["balance_id"].ToString()),
        }

        #endregion
    }
}
