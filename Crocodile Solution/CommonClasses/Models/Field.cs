﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
	class Field
	{
		#region Public Properties

		public string Content { get; set; }

		public bool IsRequired { get; set; } = false;

		public bool CanBeEmpty { get; set; } = true;

		#endregion

		#region Public Methods

		public override string ToString()
		{
			return Content;
		}

		#endregion

		#region Static Public Methods

		static public implicit operator String(Field field)
		{
			return field.ToString();
		}

		#endregion
	}
}
