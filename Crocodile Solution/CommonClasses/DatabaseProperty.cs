﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class DatabasePropertyAttribute : Attribute
    {
        #region Public Properties

        public string Name { get; }

        #endregion

        #region Life Cycle

        public DatabasePropertyAttribute(string name)
        {
            Name = name;
        }

        #endregion
    }
}
