﻿using System;
using System.IO;
using System.Threading;

namespace Common.Services
{
	public class LogService
	{
		#region Private Fields

		private bool _isInitialized = false;
		private StreamWriter _streamWriter;
		private bool _isNeedToLogInConsole = true;

        #endregion

        #region Public Properties

        public bool IsInitialized => _isInitialized;

        #endregion

        #region Public Methods

        public void SetLogInConsole(bool isNeed)
		{
			_isNeedToLogInConsole = isNeed;
		}

		public void Initialize(string fileName)
		{
			if (_isInitialized)
			{
				return;
			}

			if (_streamWriter != null)
			{
				_streamWriter.Close();
				_streamWriter = null;
			}
			FileStream FS = new FileStream(fileName, FileMode.Append, FileAccess.Write);
			_streamWriter = new StreamWriter(FS);
			_isInitialized = true;
			Log("Log file was created: {0}", fileName);
		}

		public void Log(object obj, params object[] objs)
		{
            if (!_isInitialized)
            {
                throw new Exception("Trying to call LogService.Log(object obj, params object[] objs) without being initialized");
            }

            lock (_streamWriter)
            {
                string time = DateTime.Now.ToString("HH:mm:ss");
                string str = $"[{time}] {obj}";
                if (objs.Length == 0)
                {
                    _streamWriter.WriteLine(str);
                    if (_isNeedToLogInConsole)
                    {
                        Console.WriteLine(str);
                    }
                }
                else
                {
                    _streamWriter.WriteLine(str, objs);
                    if (_isNeedToLogInConsole)
                    {
                        Console.WriteLine(str, objs);
                    }
                }
            }
		}

		public void Uninitialize()
		{
			if (_streamWriter != null)
			{
                try
                {
                    _streamWriter.Close();
                }
                catch { }
            }
		}

		#endregion
	}
}
