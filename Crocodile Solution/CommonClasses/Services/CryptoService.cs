﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Linq;

namespace Common.Services
{
	public class CryptoService
	{
		#region Private Fileds

		private string _clientPublicKey;
		private string _serverPrivateKey;
		private bool _isNeedFOAEP;

		#endregion

		#region Life Cycle

		public CryptoService(string clientPublicKey, out string serverPublicKey, bool isNeedFOAEP)
		{
			_clientPublicKey = clientPublicKey;
			_isNeedFOAEP = isNeedFOAEP;

			string xml;
			using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
			{
				xml = rsa.ToXmlString(false);
				byte[] ba = Encoding.Default.GetBytes(XElement.Parse(xml).Element(XName.Get("Exponent")).Value);
				var hexString = BitConverter.ToString(ba);
				hexString = hexString.Replace("-", "");
				serverPublicKey = XElement.Parse(xml).Element(XName.Get("Modulus")).Value + "|" + hexString;
				_serverPrivateKey = rsa.ToXmlString(true);
			}
		}

		#endregion

		#region Public Methods

		public byte[] Encrypt(byte[] data)
		{
			byte[] encryptedData;
			using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
			{
				rsa.FromXmlString(_clientPublicKey);
				encryptedData = rsa.Encrypt(data, _isNeedFOAEP);
			}
			return encryptedData;
		}

		public byte[] Decrypt(byte[] encryptedData)
		{
			byte[] data;
			using (RSACryptoServiceProvider rsa = new RSACryptoServiceProvider())
			{
				rsa.FromXmlString(_serverPrivateKey);
				data = rsa.Decrypt(encryptedData, _isNeedFOAEP);
			}
			return data;
		}

		#endregion
	}
}
