﻿using System.Collections.Generic;
using System.Data.Common;
using System.Data;
using GalaSoft.MvvmLight.Ioc;

namespace Common.Services
{
    public class DatabaseService
    {
        #region Private Fields

        private DbProviderFactory _providerFactory;
        private DbConnection _connection;
        private DbDataAdapter _adapter;
        private LogService _logService;

        #endregion

        #region Life Cycle

        public DatabaseService(string provider, string connectionString)
        {
            _providerFactory = DbProviderFactories.GetFactory(provider);
            _connection = _providerFactory.CreateConnection();
            _connection.ConnectionString = connectionString;
            _adapter = _providerFactory.CreateDataAdapter();
            _logService = SimpleIoc.Default.GetInstance<LogService>();
        }

        #endregion

        #region Public Methods

        public List<T> Select<T>(string query) where T : IQueryableObject, new()
        {
            using (DbCommand command = _connection.CreateCommand())
            {
                DataTable table = new DataTable();

                command.CommandText = query;
                _adapter.SelectCommand = command;
                _adapter.Fill(table);

                List<T> result = new List<T>();
                foreach (DataRow row in table.Rows)
                {
                    T obj = new T();
                    obj.FromDataRow(row);
                    result.Add(obj);
                }
                return result;
            }
        }

        public void Insert(string query)
        {
            _connection.Open();
            using (DbCommand command = _connection.CreateCommand())
            {
                command.CommandText = query;
                command.ExecuteNonQuery();
            }
            _connection.Close();
        }

        #endregion
    }
}
