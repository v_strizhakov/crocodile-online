﻿using Common.Extensions;
using Common.Helpers;
using Common.Models;
using Common.Queries;
using Common.Services;
using GalaSoft.MvvmLight.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Common
{
    public static class Auth
    {
        #region Static Private Fields

        static private DatabaseService _databaseService => SimpleIoc.Default.GetInstance<DatabaseService>();
        static private LogService _logService => SimpleIoc.Default.GetInstance<LogService>();

        #endregion

        #region Static Public Methods

        static public object Login(string data, string version)
        {
            Field[] requiredFileds = new Field[]
            {
                new Field { Content = "username", IsRequired = true, CanBeEmpty = false },
                new Field { Content = "password", IsRequired = true, CanBeEmpty = false }
            };
            Dictionary<string, string> fields = Validator.ValidateFromJSON(data, requiredFileds);

            switch (version)
            {
                case "0.0":
                    {
                        string username = fields["username"];
                        string password = fields["password"];

                        SelectQueryBuilder select = QueryBuilderFactory.CreateSelectQueryBuilder()
                                                    .Select("*")
                                                    .From("users")
                                                    .Where("username", "=", username)
                                                    .AndWhere("password", "=", password);

                        User user = _databaseService?.Select<User>(select.ToString()).FirstOrDefault();

                        if (user != null)
                        {
                            return user;
                        }
                        else
                        {
                            // TODO: Create InvalidUserDataException
                            throw new RequestProcessorException("Invalid username or password");
                        }
                    }
                default:
                    throw new UnknownVersionException(version);
            }
        }
        static public object Register(string data, string version)
        {
            Field[] requiredFileds = new Field[]
            {
                new Field { Content = "username", IsRequired = true, CanBeEmpty = false },
                new Field { Content = "password", IsRequired = true, CanBeEmpty = false },
                new Field { Content = "email", IsRequired = true, CanBeEmpty = false },
            };

            Dictionary<string, string> fields = Validator.ValidateFromJSON(data, requiredFileds);

            switch (version)
            {
                case "0.0":
                    {
                        string username = fields["username"];
                        string password = fields["password"];
                        string email = fields["email"];

                        InsertQueryBuilder insert = QueryBuilderFactory.CreateInsertQueryBuilder()
                                                    .InsertInto("users")
                                                    .Columns("username", "password", "email")
                                                    .Values(username, password, email);

                        try
                        {
                            _databaseService.Insert(insert.ToString());
                        }
                        catch (Exception ex)
                        {
                            _logService.Log(ex.Message);
                            throw new RequestProcessorException("User with the same username already exists");
                        }
                    }
                    break;
                default:
                    throw new UnknownVersionException(version);
            }
            return null;
        }

        #endregion
    }
}
