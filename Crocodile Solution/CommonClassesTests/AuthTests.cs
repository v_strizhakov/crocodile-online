﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Models;
using Newtonsoft.Json;
using Common.Services;
using GalaSoft.MvvmLight.Ioc;
using Common.Managers;
using Common.Extensions;

namespace Common.Tests
{
    [TestClass()]
    public class AuthTests
    {
        static private LogService _logService;

        static AuthTests()
        {
            SimpleIoc.Default.Register<LogService>(true);
            _logService = SimpleIoc.Default.GetInstance<LogService>();
            string logFileName = String.Format("{0}_{1}{2}", DateTime.Now.ToShortDateString().Replace('.', '_'), nameof(AuthTests), ConstantsManager.File.Extensions.Log);
            _logService.Initialize(logFileName);

            string provider = "System.Data.SqlClient";
            string connectionString = @"Server=tcp:crocodile000.database.windows.net,1433;Initial Catalog=Crocodile;Persist Security Info=False;User ID=vovastrigh;Password=CrocodileTemporaryPassword2019;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            SimpleIoc.Default.Register<DatabaseService>(() => new DatabaseService(provider, connectionString), true);
        }

        [TestMethod()]
        public void Register_NewUsername_Registers_Version_0_0()
        {
            User user = new User
            {
                Username = "123",
                Password = "456",
                Email = "example@gmail.com"
            };
            string jsonUserData = JsonConvert.SerializeObject(user);
            string version = "0.0";

            const string message = "Auth.Register registering new user failed";
            try
            {
                Auth.Register(jsonUserData, version);
            }
            catch (RequestProcessorException)
            {
                Assert.Fail(message);
            }
        }

        [TestMethod()]
        public void Register_ExistingUsername_DoNotRegister_Version_0_0()
        {
            User user = new User
            {
                Username = "123",
                Password = "456",
                Email = "example@gmail.com"
            };
            string jsonUserData = JsonConvert.SerializeObject(user);
            string version = "0.0";

            const string message = "Auth.Register registering existing user failed";
            try
            {
                Auth.Register(jsonUserData, version);
                Assert.Fail(message);
            }
            catch (RequestProcessorException)
            {
            }
        }

        [TestMethod()]
        public void Login_WithValidUserData_Logins_Version_0_0()
        {
            User user = new User
            {
                Username = "123",
                Password = "456"
            };
            string jsonUserData = JsonConvert.SerializeObject(user);
            string version = "0.0";

            const string message = "Auth.Login with valid user data Failed";
            try
            {
                User authorizedUser = Auth.Login(jsonUserData, version) as User;
                Assert.IsNotNull(authorizedUser, message);
            }
            catch (RequestProcessorException)
            {
                Assert.Fail(message);
            }
        }

        [TestMethod()]
        public void Login_WithInvalidUserData_DoesntLogin_Version_0_0()
        {
            User user = new User
            {
                Username = "123",
                Password = "invalidPassword"
            };
            string jsonUserData = JsonConvert.SerializeObject(user);
            string version = "0.0";

            const string message = "Auth.Login with invalid user data Failed";
            Assert.ThrowsException<RequestProcessorException>(() =>
            {
                Auth.Login(jsonUserData, version);
            }, message);
        }
    }
}