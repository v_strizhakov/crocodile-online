﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace CrocodileOnline
{
    public delegate void CultureInfoChanged(CultureInfo cultureInfo);

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region Static Private Fields

        static private List<CultureInfo> _languages = new List<CultureInfo>();

        #endregion

        #region Static Public Properties

        public static List<CultureInfo> Languages => _languages;

        public static CultureInfo Language
        {
            get => System.Threading.Thread.CurrentThread.CurrentUICulture;
            set
            {
                if (value == null)
                {
                    throw new ArgumentException("value");
                }
                if (value == System.Threading.Thread.CurrentThread.CurrentUICulture) return;

                System.Threading.Thread.CurrentThread.CurrentUICulture = value;

                ResourceDictionary dictionary = new ResourceDictionary();
                switch (value.Name)
                {
                    case "ru-RU":
                        dictionary.Source = new Uri("Resources/Languages/Russian.xaml", UriKind.Relative);
                        break;
                    case "en-US":
                    default:
                        dictionary.Source = new Uri("Resources/Languages/English.xaml", UriKind.Relative);
                        break;
                }

                ResourceDictionary oldDictionary = (from dict in Application.Current.Resources.MergedDictionaries
                                                    where dict.Source != null && dict.Source.OriginalString.StartsWith("Resources/Languanges/")
                                                    select dict).FirstOrDefault();
                if (oldDictionary != null)
                {
                    Application.Current.Resources.MergedDictionaries.Remove(oldDictionary);
                }
                Application.Current.Resources.MergedDictionaries.Add(dictionary);

                LanguageChanged?.Invoke(value);
            }
        }

        #endregion

        #region Events

        static public event CultureInfoChanged LanguageChanged;

        #endregion

        #region Life Cycle

        public App()
        {
            _languages.Clear();
            _languages.Add(new CultureInfo("en-US"));

            LoadCompleted += App_LoadCompleted;
            LanguageChanged += App_LanguageChanged;
        }

        #endregion

        #region Callbacks

        private void App_LanguageChanged(CultureInfo cultureInfo)
        {
            Settings.Default.DefaultLanguage = cultureInfo;
            Settings.Default.Save();
        }

        private void App_LoadCompleted(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            Language = Settings.Default.DefaultLanguage;
        }

        #endregion
    }
}
