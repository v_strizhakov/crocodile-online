﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace CrocodileOnline.CustomControls
{
    public class TextBoxWithPlaceholder : TextBox
    {
        #region Dependency Properties

        static public readonly DependencyProperty PlaceholderProperty =
            DependencyProperty.Register(
                nameof(Placeholder),
                typeof(string),
                typeof(TextBoxWithPlaceholder),
                new PropertyMetadata(String.Empty));

        #endregion

        #region Public Properties

        public string Placeholder
        {
            get => GetValue(PlaceholderProperty) as string;
            set => SetValue(PlaceholderProperty, value);
        }

        #endregion

        #region Private Fields

        private bool _isPlaceholder;
        private Brush _textColor;

        #endregion

        #region Life Cycle

        public TextBoxWithPlaceholder() : base()
        {
            Loaded += TextBoxWithPlaceholder_Loaded;
            Unloaded += TextBoxWithPlaceholder_Unloaded;
        }

        #endregion

        #region Callbacks

        private void TextBoxWithPlaceholder_Loaded(object sender, RoutedEventArgs e)
        {
            _textColor = Foreground;
            _isPlaceholder = !String.IsNullOrEmpty(Placeholder);
            if (_isPlaceholder)
            {
                ShowPlaceholder();
            }
            GotFocus += TextBoxWithPlaceholder_GotFocus;
            LostFocus += TextBoxWithPlaceholder_LostFocus;
        }

        private void TextBoxWithPlaceholder_Unloaded(object sender, RoutedEventArgs e)
        {
            Loaded -= TextBoxWithPlaceholder_Loaded;
            Unloaded -= TextBoxWithPlaceholder_Unloaded;
            GotFocus -= TextBoxWithPlaceholder_GotFocus;
            LostFocus -= TextBoxWithPlaceholder_LostFocus;
        }

        private void TextBoxWithPlaceholder_GotFocus(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(Placeholder))
            {
                if (_isPlaceholder)
                {
                    HidePlaceholder();
                }
            }
        }

        private void TextBoxWithPlaceholder_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(Placeholder))
            {
                if (String.IsNullOrWhiteSpace(Text))
                {
                    ShowPlaceholder();
                }
            }
        }

        #endregion

        #region Private Methods

        private void ShowPlaceholder()
        {
            Text = Placeholder;
            _textColor = Foreground;
            Foreground = new SolidColorBrush(Colors.Gray);
            _isPlaceholder = true;
        }

        private void HidePlaceholder()
        {
            Text = String.Empty;
            Foreground = _textColor;
            _isPlaceholder = false;
        }

        #endregion
    }
}
