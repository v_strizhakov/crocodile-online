﻿using System.Net.Sockets;
using Common.Protocol;
using Common;

namespace Server
{
	class RequestProcessorFactory : IRequestProcessorFactory
	{
		public IRequestProcessor CreateRequestProcessor(TcpClient client)
		{
			return new RequestProcessor(client);
		}
	}
}
