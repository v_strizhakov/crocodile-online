﻿using System;
using System.Linq;
using System.Net.Sockets;
using Common.Protocol;
using Common.Helpers;
using System.Reflection;
using Common.Extensions;
using Common.Managers;
using Common.Services;
using GalaSoft.MvvmLight.Ioc;

namespace Server
{
	public class RequestProcessor : IRequestProcessor
	{
        #region Private Fields

        private TcpClient _client;

		#endregion

		#region Life Cycle

		public RequestProcessor(TcpClient client)
		{
			_client = client;
		}

		#endregion

		#region IRequestProcessor

		public Response ProcessRequest(object requestObject)
		{
			Request request = (Request)requestObject;

			string name = request.Headers[ConstantsManager.Protocol.Header.Name].ToUpper();
			string version = request.Headers[ConstantsManager.Protocol.Header.Version];
			string methodPath = request.Headers[ConstantsManager.Protocol.Header.MethodPath].ToLower();

			if (name != ConstantsManager.Protocol.Name)
			{
				throw new UnknownProtocolNameException(name);
			}

			if (!ConstantsManager.Protocol.Versions.Contains(version))
			{
				version = ConstantsManager.Protocol.Versions.Last();
			}

			string[] classMethod = methodPath.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);

			string typeName = "Common." + NameRecreator.ToCSharpName(classMethod[0]);
			string methodName = NameRecreator.ToCSharpName(classMethod[1]);

			Type type = Type.GetType(typeName + ", CommonClasses");
			if (type == null)
			{
				throw new UnknownMethodPathException(methodPath);
			}

			MethodInfo methodInfo = type.GetMethod(methodName);
			if (methodInfo == null || methodInfo.IsPrivate)
			{
				throw new UnknownMethodPathException(methodPath);
			}

			object instance = null;
			if (!methodInfo.IsStatic)
			{
				instance = Activator.CreateInstance(type);
			}

			Response response;
			try
			{
				object responseString = methodInfo.Invoke(instance, new object[] { request.JsonData, version });
				response = Response.CreateSuccessfullWidthData(responseString);
			}
			catch (Exception ex)
			{
                Type exceptionType = ex.InnerException.GetType();

                if (exceptionType.IsSubclassOf(typeof(RequestProcessorException)) || exceptionType == typeof(RequestProcessorException))
				{
					response = Response.CreateFailureWithError(ex.InnerException.Message);
				}
				else
				{
					throw ex;
				}
			}

			return response;
		}

		#endregion
	}
}
