﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;
using Common.Services;
using Common.Managers;
using Common.Threads;
using System.Net;
using System.Data;
using GalaSoft.MvvmLight.Ioc;
using Common.Protocol;

namespace Server
{
    public class Server
    {
        #region Private Fields

        static ServerThread _serverThread;
        static DatabaseService _databaseService;
        static LogService _logService;

        #endregion

        #region Life Cycle

        static void Main(string[] args)
        {
            try
            {
                Initialize();
                Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occured while initalizing and starting the server: {0}", ex.Message);
                Console.WriteLine("Press any key to exit.");
            }
            Console.ReadKey();
            Uninitialize();
        }

        static public void Initialize()
        {
            SimpleIoc.Default.Register<LogService>(true);
            _logService = SimpleIoc.Default.GetInstance<LogService>();
            string logFileName = DateTime.Now.ToShortDateString().Replace('.', '_') + ConstantsManager.File.Extensions.Log;
            _logService.Initialize(logFileName);

            string provider = ConfigurationManager.AppSettings["provider"];
            string connectionString = ConfigurationManager.AppSettings["connectionString"];

            SimpleIoc.Default.Register<DatabaseService>(() => new DatabaseService(provider, connectionString), true);
            _databaseService = SimpleIoc.Default.GetInstance<DatabaseService>();

            SimpleIoc.Default.Register<IRequestProcessorFactory, RequestProcessorFactory>();

            string ip = ConfigurationManager.AppSettings["ip"];
            int port = Int32.Parse(ConfigurationManager.AppSettings["port"]);

            _serverThread = new ServerThread(new IPEndPoint(IPAddress.Parse(ip), port));
        }

        static public void Uninitialize()
        {
            if (_logService.IsInitialized)
            {
                _logService.Uninitialize();
            }
        }

        static void Start()
        {
            Thread thread = new Thread(_serverThread.Run);
            thread.IsBackground = true;
            thread.Start();
        }

        #endregion
    }
}
