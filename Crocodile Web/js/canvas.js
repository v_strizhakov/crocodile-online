var forEach=function(array,callback,scope){
	"use strict";
for(var i=0;i<array.length;i++)
	{callback.call(scope,i,array[i])}};
var bindEvent=function(element,type,handler){
	"use strict";
if(element.addEventListener)
	{element.addEventListener(type,handler,false)}
else{element.attachEvent("on"+type,handler)}};
var classList=function(el){
	var list=el.classList;
	return{toggle:function(c){
		list.toggle(c);return this},
		add:function(c){list.add(c);return this},
		remove:function(c){list.remove(c);return this}}};
function Particles(){
	this.colors=["110, 140, 62","51, 125, 190","227, 83, 106"];
	this.blurry=false;
	this.border=false;
	this.minRadius=2;
	this.maxRadius=6;
	this.minOpacity=.005;
	this.maxOpacity=.3;
	this.minSpeed=.05;
	this.maxSpeed=.5;
	this.fps=60;
	this.numParticles=170;
	this.canvas=document.getElementById("canvas");
	this.ctx=this.canvas.getContext("2d")}
	Particles.prototype.init=function(){
		this.render();
		this.createCircle()};
		Particles.prototype._rand=function(min,max){return Math.random()*(max-min)+min};
		Particles.prototype.render=function(){
			var self=this;
			var container=document.querySelector("#bubbles");
			var wHeight=container.offsetHeight;
			var wWidth=document.body.offsetWidth;
			self.canvas.width=wWidth;
			self.canvas.height=wHeight;
			bindEvent(window,"resize",self.render)};
		Particles.prototype.createCircle=function(){
			var particle=[];for(var i=0;i<this.numParticles;i++){
				var self=this,
				color=self.colors[~~self._rand(0,self.colors.length)];
				particle[i]={radius:self._rand(self.minRadius,self.maxRadius),
					xPos:self._rand(0,canvas.width),yPos:self._rand(0,canvas.height),
					xVelocity:self._rand(self.minSpeed,self.maxSpeed),
					yVelocity:self._rand(self.minSpeed,self.maxSpeed),
					color:"rgba("+color+","+self._rand(self.minOpacity,
						self.maxOpacity)+")"};self.draw(particle,i)}
					self.animate(particle)};
		Particles.prototype.draw=function(particle,i){
			var self=this,ctx=self.ctx;if(self.blurry===true){
				var grd=ctx.createRadialGradient(particle[i].xPos,
					particle[i].yPos,particle[i].radius,
					particle[i].xPos,particle[i].yPos,
					particle[i].radius/1.25);
				grd.addColorStop(1,particle[i].color);
				grd.addColorStop(0,"rgba(34, 34, 34, 0)");
				ctx.fillStyle=grd}else{ctx.fillStyle=particle[i].color}
				if(self.border===true){ctx.strokeStyle="#fff";
				ctx.stroke()}ctx.beginPath();
				ctx.arc(particle[i].xPos,particle[i].yPos,particle[i].radius,0,2*Math.PI,false);
				ctx.fill()};Particles.prototype.animate=function(particle){
					var self=this,ctx=self.ctx;setInterval(function(){
						self.clearCanvas();
					for(var i=0;i<self.numParticles;i++){
						particle[i].xPos+=particle[i].xVelocity;
						particle[i].yPos-=particle[i].yVelocity;
						if(particle[i].xPos>self.canvas.width+particle[i].radius||particle[i].yPos>self.canvas.height+particle[i].radius){self.resetParticle(particle,i)}
						else{self.draw(particle,i)}}},1e3/self.fps)};Particles.prototype.resetParticle=function(particle,i){var self=this;
						var random=self._rand(0,1);if(random>.5){particle[i].xPos=-particle[i].radius;particle[i].yPos=self._rand(0,canvas.height)}else{particle[i].xPos=self._rand(0,canvas.width);particle[i].yPos=canvas.height+particle[i].radius}
						self.draw(particle,i)};
						Particles.prototype.clearCanvas=function(){this.ctx.clearRect(0,0,canvas.width,canvas.height)};
						var platform=function(){"use strict";
						var OSName="generic";if(navigator.appVersion.indexOf("Win")!=-1)OSName="win";
						if(navigator.appVersion.indexOf("Mac")!=-1)OSName="mac";
						if(navigator.appVersion.indexOf("Linux")!=-1)OSName="linux";return OSName};
						function trackExtLink(link,category,action){var url=link.href;if(ga.hasOwnProperty("loaded")&&ga.loaded===true){ga("send","event",category,action,url,{hitCallback:function(){document.location=url}})}
						else{document.location=url}}
