$(window).on('load', function () {
    var $preloader = $('#preloader');
    $preloader.delay(500).fadeOut('slow');
});
$('.rating-ico').on('click', function () {
  $('.float-window').removeClass('active');
  $('#rating').addClass('active');
});

$('.info-ico').on('click', function () {
  $('.float-window').removeClass('active');
  $('#info').addClass('active');
});

$('.gallery-ico').on('click', function () {
  $('.float-window').removeClass('active');
  $('#gallery').addClass('active');
});

$('.w_close').on('click', function () {
  $('.float-window').removeClass('active');
});
